import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const userModel = new Schema({
    password: { type: String },
    email: { type: String, unique: true }
})
export default mongoose.model('users', userModel)