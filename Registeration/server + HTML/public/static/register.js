const inputEmail = document.querySelector('#register-email-input')
const inputPassword = document.querySelector('#register-password-input')
const inputBtn = document.querySelector('#Register-btn')
const apiLink = "http://localhost:5656/api/"


inputBtn.addEventListener('click', () => {
    let email = inputEmail.value;
    let password = inputPassword.value;
    let header = {
        //credentials: 'same-origin',
        method: "POST",
        mode: "cors",
        headers: {
            "Content-Type": "application/json",
        },
        body: {
            "email": email,
            "password": password
        }
    }
    console.log(header)
    console.log('Sending...')
    try {
        fetch(apiLink+"Users/", header).then( (res) => {
            console.log(res)
        });
    } catch(err) {
        console.log(err)
    }
})
