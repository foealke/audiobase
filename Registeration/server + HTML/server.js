const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();

app.use(express.static('public'))

router.get('/',function(req,res){
    res.sendFile(path.join(__dirname+'/home.html'));
})
router.get('/home',function(req,res){
    res.sendFile(path.join(__dirname+'/home.html'));
})
router.get('/register',function(req,res){
    res.sendFile(path.join(__dirname+'/register.html'));
})
router.get('/login',function(req,res){
    res.sendFile(path.join(__dirname+'/login.html'));
})
  
  app.use('/', router);
  app.listen(8080);
  
  console.log('Running at Port 8080');

